package com.hendisantika.springbootminioexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMinioExampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMinioExampleApplication.class, args);
    }

}
